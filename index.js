const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hey There!!'))

if(process.env.ENVIRONMENT === 'production') {
    app.listen(process.env.PORT, () => console.log(`Example app listening on port ${process.env.PORT}!`))
} else {
    app.listen(port, () => console.log(`Example app listening on port ${port}!`))
}
